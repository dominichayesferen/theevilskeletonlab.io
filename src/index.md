---
title: TheEvilSkeleton
description: "Welcome to my personal website! I talk about computers, GNU/Linux ecosystems, open-source, and other topics related to software."
layout: home
---

<img src="/assets/logo-512x512.png" class="icon" height="256" alt="Logo"/>
<h2 class="no-text-decoration"><code>[Tesk ~]$ cat /etc/motd <span class="cursor">█</span></code></h2>

{{ page.description }} You can look through my writeups and information about myself in the header at the top of the page.

## Contact
You can contact me in the following platforms:
- Email: [theevilskeleton@riseup.net](mailto:TheEvilSkeleton <theevilskeleton@riseup.net>)
- [\[matrix\]](https://matrix.org/): [@theevilskeleton:fedora.im](https://matrix.to/#/@theevilskeleton:fedora.im)

## Where to Find Me
You can ~~stalk~~ find me on the following platforms:
- Version control websites:
    - [Codeberg](https://codeberg.org/TheEvilSkeleton/)
    - [GitHub](https://github.com/TheEvilSkeleton)
    - [GitLab](https://gitlab.com/TheEvilSkeleton)
- <a rel="me" href="https://fosstodon.org/@TheEvilSkeleton">Mastodon</a>
