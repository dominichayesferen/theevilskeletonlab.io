---
title: "About Me"
description: "My name is Hari Rana. I'm known as TheEvilSkeleton, TheEvilSkely, Skelly, and Tesk online."
layout: about
---

<img src="/assets/logo-512x512.png" class="icon" height="256" alt="Logo"/>
<h2 class="no-text-decoration"><code>[Tesk ~]$ whoami <span class="cursor">█</span></code></h2>

{{ page.description }}

I contribute to [free and open source software](https://en.wikipedia.org/wiki/Free_and_open-source_software) as much as I can, mainly documentation, Flatpak/Flathub and Fedora. I am currently a part of the [Fedora Editorial board](https://docs.fedoraproject.org/en-US/fedora-magazine/editorial-meetings/#:~:text=chair%20glb%20rlengland-,theevilskeleton,-cverna%20asamalik) at Fedora Magazine, occasionally writing and editing articles. I am also a part of Fedora quality assurance (QA). I want to contribute to the adoption of the Linux desktop by promoting various technologies and helping people in need of assistance. I write articles as a hobby, mainly about Linux. My content are meant for educational purposes because I believe in open information, so I talk less about myself and more about technologies.

If you are curious and want to know more about what I like, **check out the [Rocks](/rocks) page**!

---

## Personal Setup

### Hardware
My laptop is a [Dell Inspiron 15 5005 (OPTION TWO)](https://web.archive.org/web/20210325222115/https://dl.dell.com/topicspdf/inspiron-15-5505-laptop_users-guide_en-us.pdf). My desktop is a custom built PC with the following specs:
- Motherboard: [MSI B350M GAMING PRO](https://www.msi.com/Motherboard/b350m-gaming-pro.html)
- RAM: [2x4GB Team T-Force Vulcan 3200MHz](https://www.teamgroupinc.com/en/product/vulcan-ddr4)
- CPU: [Ryzen 5 1600](https://www.amd.com/en/products/cpu/amd-ryzen-5-1600)
- GPU: [Sapphire Nitro+ RX 580 4GB](https://www.sapphiretech.com/en/consumer/nitro-rx-580-4g-g5)
- PSU: [EVGA 430W](https://www.evga.com/products/specs/psu.aspx?pn=3615c216-ffc7-409a-8536-7cf08f2674a2)
- Case: [MasterBox MB600L](https://www.coolermaster.com/us/en-us/catalog/cases/mid-tower/masterbox-mb600l/)

### Software

#### Operating System
At the moment, I use Fedora Silverblue as the operating system (OS) on both my laptop and desktop computer. I use [GNOME](https://www.gnome.org/) as my main desktop environment (DE), with the [GSConnect](https://extensions.gnome.org/extension/1319/gsconnect/) extension.

#### Software and Applications
I use the following software and applications frequently:
- GNOME apps (Maps, Calculator, Calendar, etc.)
- Browser: [Firefox](https://www.mozilla.org/en-US/firefox/new/)
- IDE/Text editor: [GNOME Builder](https://apps.gnome.org/app/org.gnome.Builder/) for IDE and [Commit](https://apps.gnome.org/app/re.sonny.Commit/) for Git commits
- Shell: [fish](https://fishshell.com/)
- Messaging: [Matrix](https://matrix.org/) ([Element](https://element.io/))
- RSS reader: [NewsFlash](https://apps.gnome.org/app/com.gitlab.newsflash/)
- Password manager: [Bitwarden](https://bitwarden.com/)
- Office: [LibreOffice](https://www.libreoffice.org/)
- Image editor: [GIMP](https://www.gimp.org/)
- Package managers: [rpm-ostree](https://coreos.github.io/rpm-ostree/) and [Flatpak](https://flatpak.org/)
- File hosting service: [Nextcloud](https://nextcloud.com/)

