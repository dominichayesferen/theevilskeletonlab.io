---
title: "Please Don't Unofficially Ship Bottles in Distribution Repositories"
tags: ["Linux"]
description: "We are developers and contributors of Bottles who want to provide the best support to our users."
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://usebottles.com/blog/an-open-letter/'"/>
  </head>
</html>
