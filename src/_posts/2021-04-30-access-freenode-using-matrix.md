---
title: "Access freenode Using Matrix Clients"
tags: ["IRC", "Matrix"]
description: "Matrix (also written [matrix]) is an [open source project](https://matrix.org/) and a [communication protocol](https://matrix.org/docs/spec/). The protocol standard is open and it is free to use or implement. Matrix is being recognized as a modern successor to the older [Internet Relay Chat (IRC)](https://en.wikipedia.org/wiki/Internet_Relay_Chat) protocol. [Mozilla](https://matrix.org/blog/2019/12/19/welcoming-mozilla-to-matrix/), [KDE](https://matrix.org/blog/2019/02/20/welcome-to-matrix-kde/), [FOSDEM](https://matrix.org/blog/2021/01/04/taking-fosdem-online-via-matrix) and [GNOME](https://wiki.gnome.org/Initiatives/Matrix) are among several large projects that have started using chat clients and servers that operate over the Matrix protocol. Members of the Fedora project have [discussed](https://discussion.fedoraproject.org/t/the-future-of-real-time-chat-discussion-for-the-fedora-council/24628) whether or not the community should switch to using the Matrix protocol."
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://fedoramagazine.org/access-freenode-using-matrix-clients/'"/>
  </head>
</html>
