---
title: "An introduction to Fedora Flatpaks"
tags: ["Linux", "Fedora", "Flatpak", "Containers"]
description: "Flatpak is a distribution agnostic universal package manager leveraging [bubblewrap](https://github.com/containers/bubblewrap) to separate applications from the system, and [OSTree](https://ostreedev.github.io/ostree/) to manage applications. There are multiple Flatpak repositories (remotes in Flatpak terminology), such as [Flathub](https://flathub.org/home) (the de-facto standard), [GNOME Nightly](https://wiki.gnome.org/Apps/Nightly), [KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications) and finally Fedora Flatpaks, Fedora Project’s Flatpak remote."
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://fedoramagazine.org/an-introduction-to-fedora-flatpaks/'"/>
  </head>
</html>
