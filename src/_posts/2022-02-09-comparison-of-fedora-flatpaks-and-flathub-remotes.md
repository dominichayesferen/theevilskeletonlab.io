---
title: "Comparison of Fedora Flatpaks and Flathub remotes"
tags: ["Linux", "Fedora", "Flatpak", "Containers"]
description: In the [previous article in this series](https://fedoramagazine.org/an-introduction-to-fedora-flatpaks/), we looked at how to get started with Fedora Flatpaks and how to use it. This article compares and contrasts between the Fedora Flatpaks remote and the Flathub remote. Flathub is the de-facto standard Flatpak remote, whereas Fedora Flatpaks is the Fedora Project’s Flatpak remote. The things that differ between the remotes include but are not limited to their policies, their ways of distribution, and their implementation.
---

<html lang="en">
  <head>
    <meta http-equiv="Refresh" content="0; url='https://fedoramagazine.org/comparison-of-fedora-flatpaks-and-flathub-remotes/'"/>
  </head>
</html>
